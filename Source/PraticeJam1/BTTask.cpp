// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask.h"
#include "BotTargetPoint.h"
#include "EnnemyController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Engine/Engine.h"

PRAGMA_DISABLE_OPTIMIZATION

EBTNodeResult::Type UBTTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AEnnemyController* AIController = Cast<AEnnemyController>(OwnerComp.GetAIOwner());
	//reference to ennemy character

	//If controller is valid 
		//Get Blackboard component and the current point of the bot
		//Search for the next point -- different from current point
	if (AIController)
	{
		UBlackboardComponent* blackboardComp = AIController->GetBlackboardComp();
		ABotTargetPoint* currentPoint = Cast<ABotTargetPoint>(blackboardComp->GetValueAsObject("LocationToGo"));

		TArray<AActor*> AvailableTargetPoints = AIController->GetAvailableTargetPoints();

		//this variable will contain a random index in order to determine the next possible point
		int32 randomIndex;

		// Here,we store the possible next target point

		ABotTargetPoint* nextTargetPoint = nullptr;

		//reference to controller to cast
		auto controller = Cast<AEnnemyController>(this->GetClass());

		//Find a next point which is different from the current one

		do
		{
			randomIndex = FMath::RandRange(0, AvailableTargetPoints.Num() - 1);
			//Remember that the array provided by the controller function contains AActor* objects, so we need to cast
			nextTargetPoint = Cast<ABotTargetPoint>(AvailableTargetPoints[randomIndex]);
		} while (currentPoint == nextTargetPoint);

		//Update the next location in the Blackboard so the bot can move tot he next Blackboard value
		blackboardComp->SetValueAsObject("LocationToGo", nextTargetPoint);

		//From there, it's a success so
		return EBTNodeResult::Succeeded;
	}
	return EBTNodeResult::Failed;
}


PRAGMA_ENABLE_OPTIMIZATION