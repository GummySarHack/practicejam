// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Runtime/AIModule/Classes/Perception/PawnSensingComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "EnnemiCharacter.generated.h"

UCLASS()
class PRATICEJAM1_API AEnnemiCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Awareness)
		UPawnSensingComponent* pawnSensor;
	// Sets default values for this character's properties
	AEnnemiCharacter();
	//Current Health
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Health, meta = (AllowPrivateAccess = "true"))
		float ennemyCurrentHealth = 200.f;

	//BB reference
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = AI, meta = (AllowPrivateAccess = "true"))
	UBlackboardComponent* blackBoardComp;
	//BHT reference
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = AI, meta = (AllowPrivateAccess = "true"))
	UBehaviorTreeComponent* behaviourComp;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:	

	////Bot target
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Target, meta = (AllowPrivateAccess = "true"))
		class ABotTargetPoint* AIBotTarget;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Target, meta = (AllowPrivateAccess = "true"))
		class ABotTargetPoint* AIBotTarget2;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = ID, meta = (AllowPrivateAccess = "true"))
		FString CharacterID;

	UPROPERTY(BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = "true"))
		bool IsAlive;

	UPROPERTY(BlueprintReadOnly, Category = Target, meta = (AllowPrivateAccess = "true"))
		bool stopPatrolling;

	void SetEnnemyCurrentHealth(float newhealth)
	{
		ennemyCurrentHealth = newhealth;
	};
	float GetEnnemyCurrentHealth() { return ennemyCurrentHealth; };


	UFUNCTION(BlueprintCallable)
		void OnSeePawn(APawn* SeenPawn);
	
	//access BHT
	UPROPERTY(EditAnywhere, Category = "AI")
		class UBehaviorTree* CharacterBehaviourTree;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Target, meta = (AllowPrivateAccess = "true"))
		bool IsTargetC = false;

	bool GetIsTarget() { return IsTargetC; }
	bool SetIsTarget(bool target) { return IsTargetC = target; }

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class APraticeJam1Character* sideCharacterplayer;

	UPROPERTY(BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class AEnnemiProjectile* Projectile;
};
