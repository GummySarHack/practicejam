// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameInstance.h"
#include "MySaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"

void UMyGameInstance::SaveGame()
{ 
	UMySaveGame* saveGameInstance = Cast<UMySaveGame>
									(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
	saveGameInstance->coinCollectedTotal = numberOfCoins;
	saveGameInstance->playerPosition = playerPositionGI;
	saveGameInstance->takenCoins = takenCoinPositions;
	saveGameInstance->healthPlayer = playerHealth;
	saveGameInstance->EnemiesDestroyed = EnemiesDestroyed;
	saveGameInstance->levelPlaying = UGameplayStatics::GetCurrentLevelName(this);

	UGameplayStatics::SaveGameToSlot(saveGameInstance, "GameSlot", 0);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("SaveGameInC++"));
}

void UMyGameInstance::LoadGame()
{
	IsGameLoaded = true;
	UMySaveGame* loadGameInstance = Cast<UMySaveGame>
									(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
	loadGameInstance = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot("GameSlot", 0));
	numberOfCoins = loadGameInstance->coinCollectedTotal;
	playerPositionGI = loadGameInstance->playerPosition;
	takenCoinPositions = loadGameInstance->takenCoins;
	currentLevel = loadGameInstance->levelPlaying;
	playerHealth = loadGameInstance->healthPlayer;
	EnemiesDestroyed = loadGameInstance->EnemiesDestroyed;
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("LoadGameInC++"));
}

void UMyGameInstance::ClearGame()
{
	UMySaveGame* saveGameInstance = Cast<UMySaveGame>
		(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));

	UGameplayStatics::SaveGameToSlot(saveGameInstance, "GameSlot", 0);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("ClearGameInC++"));
}