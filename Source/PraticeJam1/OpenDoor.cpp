// Fill out your copyright notice in the Description page of Project Settings.


#include "OpenDoor.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	ownerDoor = GetOwner();

	initialRotation = ownerDoor->GetActorRotation();
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// pull the trigger volume
	if (GetTotalMassOfActorOnPlate() > 50.0f)
	{
		onOpenRequest.Broadcast();
		lastDoorTime = GetWorld()->GetTimeSeconds();
	}

	//check if time to close the door
	if (GetWorld()->GetTimeSeconds() - lastDoorTime > doorCloseDelay)
	{
		onCloseRequest.Broadcast();
	}
}

float UOpenDoor::GetTotalMassOfActorOnPlate()
{
	float totalMass = 0.f;

	TArray<AActor*> overLappingActor;

	//Find the overlapping actor
		//iterate through them and add their masses

	pressurePlate->GetOverlappingActors(overLappingActor);

	for (auto actor : overLappingActor)
	{
		totalMass += actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}
	return totalMass;
}

