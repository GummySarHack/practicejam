// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerUp.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "PraticeJam1Character.h"
// Sets default values
APowerUp::APowerUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create instances
	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionHealth"));
}

// Called when the game starts or when spawned
void APowerUp::BeginPlay()
{
	Super::BeginPlay();
	
	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &APowerUp::OnBoxBeginOverlap);
	sideCharacterplayer = Cast<APraticeJam1Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

// Called every frame
void APowerUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APowerUp::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	//if the actor's location is equal to an objectDamage

	if (sideCharacterplayer == OtherActor)
	{
		IncreaseHealth();
	}
}

void APowerUp::IncreaseHealth()
{
	float health;

	//checking if character is valid
	if (sideCharacterplayer)
	{
		health = sideCharacterplayer->GetCurrentHealth();

		health = health + healthRate;

		sideCharacterplayer->SetCurrentHealth(health);

		Destroy();
	}
}