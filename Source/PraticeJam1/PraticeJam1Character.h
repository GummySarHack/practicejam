// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PraticeJam1Character.generated.h"

UCLASS(config=Game)
class APraticeJam1Character : public ACharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	//Current Health
	UPROPERTY(BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		float CurrentHealth = 100.f;	
	
	//Max Health
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health, meta = (AllowPrivateAccess = "true"))
		float MaxHealth = 100.f;

	//Projectile variables
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* spawning;

	UPROPERTY(EditAnywhere, BLueprintReadWrite, Category = BCM, meta = (AllowPrivateAccess = "true"))
		bool isCMovemenent;
protected:

	virtual void BeginPlay() override;

	//Fires a projectile
	void Fire();

	/** Called for side to side input */
	void MoveRight(float Val);

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

public:

	virtual void Tick(float delta) override;

	APraticeJam1Character();

	//meta = (DisplayName = "IsCM")

	//BMovement 
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		bool IsCM();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void JumpAction();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void MoveUpAction(float axisValue);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void UpdateCustomMovementAction(float deltaTime);


	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	float GetCurrentHealth();

	UFUNCTION(BlueprintCallable)
	float SetCurrentHealth(float newHealth);

	float GetMaxHealth() { return MaxHealth; };

	//For Number of coin UI
	UFUNCTION(BlueprintCallable)
		int GetNbCoin();
	
	void SetNbCoins(int newNBCoin);

	void SetPlayerPosition();

	//projectile class
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class AEnnemiProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class USoundBase* FireSound;

	UPROPERTY(BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class APraticeJam1Character* Characterplayer;
};
