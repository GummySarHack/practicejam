// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnnemiCharacter.h"
#include "BotTargetPoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "EnnemyController.generated.h"

/**
 * 
 */
UCLASS()
class PRATICEJAM1_API AEnnemyController : public AAIController
{
	GENERATED_BODY()

private:

	//BHT reference
	UBehaviorTreeComponent* behaviourComp;

	//BB reference
	UBlackboardComponent* blackBoardComp;

	//BB key
	UPROPERTY(EditDefaultsOnly, Category = "AI")
		FName LocationToGo;

	UPROPERTY(EditDefaultsOnly, Category = "AttackPlayer")
		FName CanAttack;

	UPROPERTY(EditAnywhere, Category = "Move")
		bool IsIdle;

	//use an actor template to use GetAllActorsOfClass()
	//save BotTargetPoint
	TArray<AActor*> botTargetPoints;

	//override Possess()
	virtual void OnPossess(APawn* pawn) override;

public:

	//constructor
	AEnnemyController();


	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	class UBehaviorTree* BehaviorTree;


	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return blackBoardComp; }

	FORCEINLINE TArray<AActor*> GetAvailableTargetPoints() const { return botTargetPoints; }

	FORCEINLINE void SetIsIdle(bool newIsIdle) { IsIdle = newIsIdle; }
};
