// Fill out your copyright notice in the Description page of Project Settings.


#include "EnnemiProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "Engine/Engine.h"


// Sets default values
AEnnemiProjectile::AEnnemiProjectile()
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");

	// set up a notification for when this component hits something blocking
	CollisionComp->OnComponentHit.AddDynamic(this, &AEnnemiProjectile::OnHit);		

	// Set as root component
	RootComponent = CollisionComp;


	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->InitialSpeed = 800;
	ProjectileMovement->MaxSpeed = 4500;

	// Die after 3 seconds by default
	InitialLifeSpan = 1;
}

void AEnnemiProjectile::BeginPlay()
{ 
	Super::BeginPlay(); 
}

void AEnnemiProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnnemiProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Destroy();
}