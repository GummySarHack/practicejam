// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine.h"
#include "Delegates/Delegate.h"
#include "OpenDoor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOpenRequest);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCloseRequest);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PRATICEJAM1_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable)
		FOnOpenRequest onOpenRequest;

	UPROPERTY(BlueprintAssignable)
		FOnCloseRequest onCloseRequest;

private:

	bool bWantsBeginPlay;
	
	//open door angle
	UPROPERTY(EditAnywhere)
		float openAngle = -85.0f;

	//Get the trigger volume to open door
	UPROPERTY(EditAnywhere)
		ATriggerVolume * pressurePlate;

	//self-explanatory
	UPROPERTY(EditAnywhere)
		float doorCloseDelay = 2.f;
	float lastDoorTime;

	AActor* ownerDoor;

	FRotator initialRotation;

	//return total mass on trigger volume
	float GetTotalMassOfActorOnPlate();
};
