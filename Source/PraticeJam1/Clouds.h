// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/AIModule/Classes/Perception/PawnSensingComponent.h"
#include "Clouds.generated.h"

UCLASS()
class PRATICEJAM1_API AClouds : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AClouds();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Awareness)
		UPawnSensingComponent* pawnSensor;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Set the amount of power given to the character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtocted = "true"))
		float BatteryPower;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* boxComponent;

	UFUNCTION()
		void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
			bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class APraticeJam1Character* sideCharacterplayer;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void spawnElectricity();
};
