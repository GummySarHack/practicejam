// Fill out your copyright notice in the Description page of Project Settings.


#include "EnnemyPawn.h"
#include "Engine/World.h"
#include "Engine/Engine.h"

// Sets default values
AEnnemyPawn::AEnnemyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	direction = 2;
}

// Called when the game starts or when spawned
void AEnnemyPawn::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEnnemyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnnemyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

