// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObjectDamage.generated.h"

UCLASS()
class PRATICEJAM1_API AObjectDamage : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObjectDamage();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, 
			bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* boxComponent;

	UPROPERTY(BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class AObjectDamage* objectDamage;

	UPROPERTY(BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class APraticeJam1Character* sideCharacterplayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Damage)
		float damage;

	void CausesDamage();
};
