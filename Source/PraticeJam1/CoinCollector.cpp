// Fill out your copyright notice in the Description page of Project Settings.


#include "CoinCollector.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "MyGameInstance.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "PraticeJam1Character.h"


// Sets default values
ACoinCollector::ACoinCollector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionDamage"));
}

// Called when the game starts or when spawned
void ACoinCollector::BeginPlay()
{
	Super::BeginPlay();
	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &ACoinCollector::OnBoxBeginOverlap);
	sideCharacterplayer = Cast<APraticeJam1Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

	if (auto gameInstanceCoin = Cast<UMyGameInstance>(GetGameInstance()))
	{
		auto indexFound = gameInstanceCoin->takenCoinPositions.Find(GetActorLocation());
		if (indexFound != INDEX_NONE)
		{
			Destroy();
		}
	}
}

// Called every frame
void ACoinCollector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACoinCollector::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
										int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (sideCharacterplayer == OtherActor)
	{
		AddCoins();
		if (auto gameInstanceCoin = Cast<UMyGameInstance>(GetGameInstance()))
		{
			gameInstanceCoin->takenCoinPositions.Add(GetActorLocation());
		}
	}
}

void ACoinCollector::AddCoins()
{
	int addedCoin;

	if (sideCharacterplayer)
	{
		addedCoin = sideCharacterplayer->GetNbCoin();
		addedCoin = addedCoin + coin;
		sideCharacterplayer->SetNbCoins(addedCoin);

		Destroy();
	}
}

