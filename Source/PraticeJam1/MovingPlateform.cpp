// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingPlateform.h"
#include "Engine/Engine.h"
#include "Engine/World.h"

// Sets default values
AMovingPlateform::AMovingPlateform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//mesh = CreateDefaultSubobject<UStaticMeshComponent>("My mesh");
	direction = 1;
}

// Called when the game starts or when spawned
void AMovingPlateform::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMovingPlateform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//creating a new location
	FVector newLocation = GetActorLocation();

	//changes the direction
	if (newLocation.Z > 1000)
	{
		direction = direction * -1;
	}

	if (newLocation.Z < 900	)
	{
		direction = direction * -1;
	}

	newLocation.Z += direction * 0.5f;;
	
	SetActorLocation(newLocation);
}

