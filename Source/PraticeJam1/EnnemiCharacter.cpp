// Fill out your copyright notice in the Description page of Project Settings.


#include "EnnemiCharacter.h"
#include "EnnemyController.h"
#include "PraticeJam1Character.h"
#include "EnnemiProjectile.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Engine/World.h"
#include "Engine/Engine.h"

#include "MySaveGame.h"

//PRAGMA_DISABLE_OPTIMIZATION

// Sets default values
AEnnemiCharacter::AEnnemiCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Projectile = CreateDefaultSubobject<AEnnemiProjectile>(TEXT("Projectile"));

	pawnSensor = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn Sensor"));
	pawnSensor->SensingInterval = .25f; // 4 times per second
	pawnSensor->bOnlySensePlayers = false;
	pawnSensor->SetPeripheralVisionAngle(75.f);
	this->GetMesh()->BodyInstance.bLockXRotation = true;
	//Creating the Delegate Functions

	blackBoardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("blackBoardComp"));
	behaviourComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("behaviourComp"));
}

// Called when the game starts or when spawned
void AEnnemiCharacter::BeginPlay()
{
	Super::BeginPlay();

//	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &AEnnemiCharacter::OnBoxBeginOverlap);
	sideCharacterplayer = Cast<APraticeJam1Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	Projectile = Cast<AEnnemiProjectile>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

	//check if exists in GameInstance
	UMySaveGame* LoadGameInstance = Cast<UMySaveGame>
		(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
	LoadGameInstance = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot("GameSlot", 0));

	if (LoadGameInstance->EnemiesDestroyed.
		FindByPredicate([&](FString& characterID) 
			{
				return characterID == this->CharacterID;
			}))
	{
		Destroy();
	}


	FScriptDelegate fScriptDelegate;
	fScriptDelegate.BindUFunction(this, "OnSeePawn");
	pawnSensor->OnSeePawn.AddUnique(fScriptDelegate);
}

// Called every frame
void AEnnemiCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnnemiCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AEnnemiCharacter::OnSeePawn(APawn* OtherPawn)
{
	auto playerPawn = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	AEnnemyController* EnemyController = Cast<AEnnemyController>(GetController());
	auto BlackboardComp = EnemyController->GetBlackboardComponent();

	if (OtherPawn == playerPawn)
	{
		IsTargetC = true;

		BlackboardComp->SetValueAsObject("Player", playerPawn);
		BlackboardComp->SetValueAsBool("Attack?", true);
	}
}


//PRAGMA_ENABLE_OPTIMIZATION