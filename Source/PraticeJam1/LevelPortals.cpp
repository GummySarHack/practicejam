// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelPortals.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "MyGameInstance.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "PraticeJam1Character.h"

// Sets default values
ALevelPortals::ALevelPortals()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionDamage"));
}

// Called when the game starts or when spawned
void ALevelPortals::BeginPlay()
{
	Super::BeginPlay();
	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &ALevelPortals::OnBoxBeginOverlap);
	sideCharacterplayer = Cast<APraticeJam1Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

// Called every frame
void ALevelPortals::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALevelPortals::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (sideCharacterplayer)
	{
		SetNextLevel(nextLevel);
		UGameplayStatics::OpenLevel(GetWorld(), (*nextLevel));
	}
}

void ALevelPortals::SetNextLevel(FString level)
{
	if (auto gameInstanceLevel = Cast<UMyGameInstance>(GetGameInstance()))
	{
		gameInstanceLevel->currentLevel = level;
		gameInstanceLevel->IsGameLoaded = false;
	}
}

FString ALevelPortals::GetNextLevel()
{
	if (auto gameInstanceLevel = Cast<UMyGameInstance>(GetGameInstance()))
	{
		gameInstanceLevel->currentLevel;
	}
	return FString();
}