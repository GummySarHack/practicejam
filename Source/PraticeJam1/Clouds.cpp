// Fill out your copyright notice in the Description page of Project Settings.


#include "Clouds.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "PraticeJam1Character.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Engine/Engine.h"

// Sets default values
AClouds::AClouds()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionDamage"));
	BatteryPower = 150.f;
}


// Called when the game starts or when spawned
void AClouds::BeginPlay()
{
	Super::BeginPlay();

	sideCharacterplayer = Cast<APraticeJam1Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &AClouds::OnBoxBeginOverlap);
}

// Called every frame
void AClouds::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AClouds::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
								UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, 
								bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == sideCharacterplayer)
	{
		Destroy();
	}
}