// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeTypes.h"
#include "BTTask.generated.h"

/**
 * 
 */
UCLASS()
class PRATICEJAM1_API UBTTask : public UBTTaskNode
{
	GENERATED_BODY()

public:
	//contains logic for current task
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};