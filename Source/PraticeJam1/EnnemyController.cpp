// Fill out your copyright notice in the Description page of Project Settings.

#include "EnnemyController.h"
#include "BotTargetPoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"

//PRAGMA_DISABLE_OPTIMIZATION

AEnnemyController::AEnnemyController()
{
	//initialising my variables
	behaviourComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("behaviourComp"));

	blackBoardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("blackBoardComp"));

	// /Script/AIModule.BehaviorTree'/Game/AI/BehaviorTreeAI.BehaviorTreeAI'

	static ConstructorHelpers::FObjectFinder< UBehaviorTree > EnemyBehaviorTreeReference
							(TEXT("/Script/AIModule.BehaviorTree'/Game/AI/BehaviorTreeAI.BehaviorTreeAI'"));
	
	BehaviorTree = EnemyBehaviorTreeReference.Object;

	LocationToGo = "LocationToGo"; 
}

void AEnnemyController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);

	//Get possessed character and check if player character is valid
	AEnnemiCharacter* AICharacter = Cast<AEnnemiCharacter>(pawn);

	botTargetPoints.Empty();
	botTargetPoints.Push(AICharacter->AIBotTarget);
	botTargetPoints.Push(AICharacter->AIBotTarget2);

	bool das = true;

	if (AICharacter)
	{
		 //if the blackboard is valid, initialise blackboard for corresponding behaviour tree
		if (AICharacter->CharacterBehaviourTree->BlackboardAsset)
		{
			blackBoardComp->InitializeBlackboard(*(AICharacter->CharacterBehaviourTree->BlackboardAsset));
			das = AICharacter->blackBoardComp->InitializeBlackboard(*(AICharacter->CharacterBehaviourTree->BlackboardAsset));
		}
	}
	//start behaviour tree with specific character
	behaviourComp->StartTree(*AICharacter->CharacterBehaviourTree);
}

//PRAGMA_ENABLE_OPTIMIZATION