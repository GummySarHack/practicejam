// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PraticeJam1GameMode.generated.h"

UCLASS(minimalapi)
class APraticeJam1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APraticeJam1GameMode();
};



