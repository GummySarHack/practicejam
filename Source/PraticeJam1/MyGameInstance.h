// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

/**
 *  This is my game instance!!
 */
UCLASS()
class PRATICEJAM1_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 numberOfCoins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float playerHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector playerPositionGI;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString currentLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FString> EnemiesDestroyed;

	UFUNCTION(BlueprintCallable)
		void SaveGame();

	UFUNCTION(BlueprintCallable)
		void LoadGame();

	UFUNCTION(BlueprintCallable)
		void ClearGame();

	bool IsGameLoaded = false;

	TArray<FVector> takenCoinPositions;
};
