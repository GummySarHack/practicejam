// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "MySaveGame.generated.h"

/**
 * 
 */
UCLASS()
class PRATICEJAM1_API UMySaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, Category = Basic)
		FString saveGameSlot;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		uint32 coinCollectedTotal;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		float healthPlayer;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		uint32 userIndex = 0;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		FVector playerPosition;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		FString levelPlaying;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		TArray<FVector> takenCoins;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		TArray<FString> EnemiesDestroyed;
};
