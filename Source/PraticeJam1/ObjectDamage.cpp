// Fill out your copyright notice in the Description page of Project Settings.


#include "ObjectDamage.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "PraticeJam1Character.h"

// Sets default values
AObjectDamage::AObjectDamage()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionDamage"));
}

// Called when the game starts or when spawned
void AObjectDamage::BeginPlay()
{
	Super::BeginPlay();
	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &AObjectDamage::OnBoxBeginOverlap);
	sideCharacterplayer = Cast<APraticeJam1Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

}

// Called every frame
void AObjectDamage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AObjectDamage::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
									UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, 
									bool bFromSweep, const FHitResult& SweepResult)
{
	//if the actor's location is equal to an objectDamage
	if (sideCharacterplayer == OtherActor)
	{
		CausesDamage();
	}
}

void AObjectDamage::CausesDamage()
{
	//TODO much love
	float health;
	
	if (sideCharacterplayer)
	{
		health = sideCharacterplayer->GetCurrentHealth();

		health = health - damage;

		sideCharacterplayer->SetCurrentHealth(health);
		
		if (sideCharacterplayer->GetCurrentHealth() <= 0)
		{
			sideCharacterplayer->Destroy();
			//aggressive code to quit game
			//FGenericPlatformMisc::RequestExit(false);
			GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
		}
	}
	
}

