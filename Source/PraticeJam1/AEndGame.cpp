// Fill out your copyright notice in the Description page of Project Settings.


#include "AEndGame.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "PraticeJam1Character.h"

// Sets default values
AAEndGame::AAEndGame()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionDamage"));

}

// Called when the game starts or when spawned
void AAEndGame::BeginPlay()
{
	Super::BeginPlay();
	
	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &AAEndGame::OnBoxBeginOverlap);
	sideCharacterplayer = Cast<APraticeJam1Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}
																						
// Called every frame
void AAEndGame::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AAEndGame::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	//if the actor's location is equal to an objectDamage
	OpenEndGameLevel();
}

void AAEndGame::OpenEndGameLevel()
{
	if (sideCharacterplayer)
	{
		UGameplayStatics::OpenLevel(GetWorld(), "EndGameLevel");
	}
}

