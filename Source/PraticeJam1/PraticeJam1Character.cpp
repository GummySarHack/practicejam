// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PraticeJam1Character.h"
#include "EnnemiProjectile.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Actor.h"
#include "MyGameInstance.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "Runtime/Core/Public/Misc/OutputDeviceNull.h"

void APraticeJam1Character::Tick(float delta)
{
	Super::Tick(delta);

	this->IsCM();
	if (!IsCM())
	{
		// Get the current location  
		FVector ActorLocation = GetActorLocation();
		// Move it slightly  
		ActorLocation.X = 100.0f;
		SetActorLocation(ActorLocation,false);
	}
	SetPlayerPosition();
}

APraticeJam1Character::APraticeJam1Character()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f, 180.f, 0.f));

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	//GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
	
	//create a spawning location
	spawning = CreateDefaultSubobject<USceneComponent>(TEXT("spawnLocation"));
	spawning->SetupAttachment(RootComponent);
}	

void APraticeJam1Character::BeginPlay()
{
	Super::BeginPlay();

	if (auto gameInstance = Cast<UMyGameInstance>(GetGameInstance()))
	{
		if (gameInstance->IsGameLoaded)
		{
			SetActorLocation(gameInstance->playerPositionGI);
			CurrentHealth = gameInstance->playerHealth;
		}
	}
}

float APraticeJam1Character::GetCurrentHealth()
{
	return CurrentHealth;
}

float APraticeJam1Character::SetCurrentHealth(float newHealth)
{
	if (auto gameInstanceCoins = Cast<UMyGameInstance>(GetGameInstance()))
	{
		gameInstanceCoins->playerHealth = newHealth;
	}
	return CurrentHealth = newHealth;
}

int APraticeJam1Character::GetNbCoin()
{
	if (auto gameInstanceCoins = Cast<UMyGameInstance>(GetGameInstance()))
	{
		return gameInstanceCoins->numberOfCoins;
	}
	return -1;
}

void APraticeJam1Character::SetNbCoins(int newNBCoin)
{
	if (auto gameInstanceCoins = Cast<UMyGameInstance>(GetGameInstance()))
	{
		gameInstanceCoins->numberOfCoins = newNBCoin;
	}
}

void APraticeJam1Character::SetPlayerPosition()
{
	if (auto gameInstancePP = Cast<UMyGameInstance>(GetGameInstance()))
	{
		gameInstancePP->playerPositionGI = GetActorLocation();
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void APraticeJam1Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAxis("MoveRight", this, &APraticeJam1Character::MoveRight);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &APraticeJam1Character::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &APraticeJam1Character::TouchStopped);
	
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APraticeJam1Character::Fire);
}

void APraticeJam1Character::Fire()
{
	const FRotator spawnRotation = spawning->GetComponentRotation();
	const FVector spawnLocation = spawning->GetComponentLocation();

	auto variable = GetWorld()->SpawnActor<AEnnemiProjectile>(ProjectileClass, spawnLocation, spawnRotation);

	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}
}

void APraticeJam1Character::MoveRight(float Value)
{
	// add movement in that direction
	if (IsCM())
	{
		MoveUpAction(Value);
	}
	else
	{
		AddMovementInput(FVector(0.f,-1.f,0.f), Value);
	}
}

void APraticeJam1Character::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// jump on any touch
	if (IsCM())
	{
		JumpAction();
	}
	else
	{
		Jump();
	}
}

void APraticeJam1Character::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	StopJumping();
}