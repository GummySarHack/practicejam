# PracticeJam1

-- the name was picked as it was supposed to be for a gaming jam but it ended being my project to show off for my internship interviews.


-- made improvements in animations + mechanics

I wanted to create a little plateformer to push my knowledge in UE4.

I learned how to use UE4 animations. I got assets from UE4 market and blended some animations together to make a character simultaneously walk towards the player and hit him.

The point is to get to the end of the third level.

Enemies will try to kill the player with their swords. They have an health bar. Each hit decreases the player (or enemy if hit). If they health bar empties, they die in a sweet dropdead animation. 

There are 3 levels. (1- trees and cartoon style, 2- steampunk, 3- tron and neons)

There are health items so the player can recover if his bar is low.
There are coins for the player to collect.

I used UE5' settings to make some moveable plateforms and rotating coins.

The little game includes an simple UI menu with a start where previously saved option.
    The save options creates a game slot to save player position, level, current health, number of coins taken, enemies destroyed.

I integrated an UE5 module which allowed my player to climb objects to add to the complexity.
    Add character movement module that at the time, I thought was cool



This game projet helped me get my first internship :D
